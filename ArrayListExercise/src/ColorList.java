import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ColorList {
    public static void main(String[] args) {
        ArrayList <String> colors = new ArrayList<>(10);
        List<String> colorCopy = new ArrayList<>();
        colors.add("blue");
        colors.add("red");
        colors.add("black");
        colors.add("white");
        colors.add("orange");
        colorCopy.add("1");
        colorCopy.add("2");
        colorCopy.add("3");
        colorCopy.add("4");
        colorCopy.add("5");
        System.out.println(colors);
        for (String color : colors){
            System.out.println(color);
        }
        colors.add(0,"yellow");
        System.out.println(colors);
        System.out.println(colors.get(3));
        colors.set(5,"pink");
        System.out.println(colors);
        colors.remove(2);
        System.out.println("List before sorting = "+colors);
        System.out.println("Index of White color = "+colors.indexOf("white"));
        Collections.sort(colors);
        System.out.println("List after sorting = "+colors);
        System.out.println("Before copy = "+colorCopy);
        Collections.copy(colorCopy, colors);
        System.out.println("After copy = "+colorCopy);
        Collections.shuffle(colors);
        System.out.println("Shuffled List = "+colors);
        Collections.reverse(colorCopy);
        System.out.println("Reverse the sorted list = "+colorCopy);
        List<String> extraction = colors.subList(0,2);
        System.out.println("Sublist of the color list = "+extraction);
        colors.add(3,"violet");
        ArrayList<Boolean> equal = new ArrayList<>();
        for(String s : colors){
            equal.add(colorCopy.contains(s));
        }
        System.out.println(colors);
        System.out.println(equal);
        Collections.swap(colors,0,1);
        System.out.println("After swap = "+colors);
        ArrayList<String> extra = new ArrayList<>();
        extra.add("light blue");
        extra.add("light purple");
        extra.add("light red");
        colors.addAll(extra);
        System.out.println(colors);
        ArrayList<String> colorClone = (ArrayList<String>) colors.clone();
        System.out.println("Cloned List = "+colorClone);
        colorClone.removeAll(colorClone);
        System.out.println("After emptying the list = "+colorClone);
        System.out.println(colorClone.isEmpty());
        colors.trimToSize();
        System.out.println("Trimmed list = "+colors);
        colors.ensureCapacity(20);
        System.out.println("Array list with increased size = "+colors);
        colors.set(0,"deep blue");
        System.out.println(colors);
        System.out.println("Print using index number...");
        for (int i=0;i<colors.size();i++){
            System.out.println(colors.get(i));
        }
    }
}
