import java.util.HashMap;
import java.util.Map;

public class HashMapPractice {
    public static void main(String[] args) {
        HashMap<Integer, String> hash = new HashMap<Integer, String>();
        hash.put(1,"First");
        hash.put(2,"Second");
        hash.put(3,"Third");
        hash.put(4,"Fourth");
        hash.put(5,"Fifth");

        for(Map.Entry en:hash.entrySet()){
            System.out.println(en.getKey()+" "+en.getValue());
        }

        System.out.println("Size of the map = "+hash.size());

        HashMap<Integer, String> hashCopy = new HashMap<Integer, String>();
        System.out.println("Before copy = "+hashCopy);
        hashCopy.putAll(hash);
        System.out.println("After copy = "+hashCopy);
        hashCopy.clear();
        System.out.println("After removing all the mappings = "+hashCopy);
        System.out.println("Is the map empty = "+hashCopy.isEmpty());

        HashMap<Integer, String> hashClone = new HashMap<Integer, String>();
        hashClone = (HashMap<Integer, String>) hash.clone();
        System.out.println("Cloned map = "+hashClone);

        if(hash.containsKey(2)) System.out.println(hash.get(2));
        else System.out.println("No mapping for this key!");

        if(hash.containsKey(8)) System.out.println(hash.get(8));
        else System.out.println("No mapping for the key!");

        if(hash.containsValue("Fourth")) System.out.println("Value exists");
        else System.out.println("No mapping for this value!");

        if(hash.containsValue("Sixth")) System.out.println("Value exists");
        else System.out.println("No mapping for the value!");

        System.out.println("Set view of the map = "+hash.entrySet());

        System.out.println("The value under key 5 = "+hash.get(5));

        System.out.println("Set view of the keys = "+hash.keySet());

        System.out.println("Collection view of the values = "+hash.values());
    }
}
