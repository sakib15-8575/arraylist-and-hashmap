package Exercise;

import java.io.*;
import java.util.Scanner;
import java.util.Vector;

public class ListOfNumbers {
    private Vector victor;
    private static final int size = 10;

    public ListOfNumbers () {
        victor = new Vector(size);
        for (int i = 0; i < size; i++)
            victor.addElement(i);
    }
    public void writeList() {
        PrintStream out = null;

        try {
            System.out.println("Entering try statement");
            out = new PrintStream(new FileOutputStream("OutFile.txt"));

            for (int i = 0; i < size; i++)
                out.println("Value at: " + i + " = " + victor.elementAt(i));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Caught ArrayIndexOutOfBoundsException: " +
                    e.getMessage());
        } catch (IOException e) {
            System.err.println("Caught IOException: " + e.getMessage());
        } finally {
            if (out != null) {
                System.out.println("Closing PrintStream");
                out.close();
            } else {
                System.out.println("PrintStream not open");
            }
        }
    }

    public void readList(String fileName){
        try {
            Scanner scanner = new Scanner(new File(fileName));
            while(scanner.hasNextInt()){
                Integer i = scanner.nextInt();
                System.out.println(i);
                victor.addElement(i);
            }
        }catch (FileNotFoundException fnf){
            System.err.println("File: "+fileName+" not found.");
        }
    }
}